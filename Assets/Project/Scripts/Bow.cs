﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour
{

	public GameObject ArrowObject;
	
	// Use this for initialization
	public void Shoot()
	{
		GameObject arrowObject = Instantiate(ArrowObject);
		arrowObject.transform.position = transform.position + transform.forward;
		arrowObject.transform.forward = transform.forward;
	}
}
