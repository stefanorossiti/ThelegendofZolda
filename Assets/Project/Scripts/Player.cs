﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata;
using JetBrains.Annotations;
using Project.Scripts;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class Player : MonoBehaviour
{
    [Header("Visual")] 
    public GameObject Model;

    [Header("Movement")] 
    public float MovingVelocity = 5f;
    public float JumpingVelocity = 5f;
    public float RotatingSpeed = 10f;

    [Header("Equipment")] 
    public Sword Sword;
    public Bomb BombPrefabs;
    public float BombThrowingSpeed = 50f;
    public Bow Bow;

    private bool _canJump;
    Rigidbody _playerRigidBody;
    private Quaternion _targetModelRotation;

    // Use this for initialization
    void Start()
    {
        Bow.gameObject.SetActive(false);
        _playerRigidBody = GetComponent<Rigidbody>();
        _targetModelRotation = Quaternion.Euler(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Model.transform.rotation =
            Quaternion.Lerp(Model.transform.rotation, _targetModelRotation, RotatingSpeed * Time.deltaTime);

        ProcessInput();

        SetIfCanJump();

        Jump();
    }

    /// <summary>
    /// Gestione del movimenti nel piano XZ
    /// </summary>
    private void ProcessInput()
    {
        _playerRigidBody.velocity = new Vector3(
            0,
            _playerRigidBody.velocity.y,
            0
        );
        
        /// Destra 
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _playerRigidBody.velocity = new Vector3(
                MovingVelocity,
                _playerRigidBody.velocity.y,
                _playerRigidBody.velocity.z
            );

            //Model.transform.rotation = Quaternion.Lerp( Model.transform.rotation, Quaternion.Euler( 0, 90, 0), RotatingSpeed );
            _targetModelRotation = Quaternion.Euler(0, 90, 0);
        }

        ///Sinistra
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            _playerRigidBody.velocity = new Vector3(
                -MovingVelocity,
                _playerRigidBody.velocity.y,
                _playerRigidBody.velocity.z
            );

//			Model.transform.rotation = Quaternion.Lerp( Model.transform.rotation, Quaternion.Euler( 0, 270, 0), RotatingSpeed );
            _targetModelRotation = Quaternion.Euler(0, 270, 0);
        }

        ///Avanti
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            _playerRigidBody.velocity = new Vector3(
                _playerRigidBody.velocity.x,
                _playerRigidBody.velocity.y,
                MovingVelocity
            );

//			Model.transform.rotation = Quaternion.Lerp( Model.transform.rotation, Quaternion.Euler( 0, 0, 0), RotatingSpeed );
            _targetModelRotation = Quaternion.Euler(0, 0, 0);
        }

        ///Indietro
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            _playerRigidBody.velocity = new Vector3(
                _playerRigidBody.velocity.x,
                _playerRigidBody.velocity.y,
                -MovingVelocity
            );

//			Model.transform.rotation = Quaternion.Lerp( Model.transform.rotation, Quaternion.Euler( 0, 180, 0), RotatingSpeed );
            _targetModelRotation = Quaternion.Euler(0, 180, 0);
        }

        //attacco con spada
        if (Input.GetMouseButtonDown((int) MouseButton.LeftMouse))
        {
            Bow.gameObject.SetActive(false);
            Sword.gameObject.SetActive(true);
            Sword.Attack();
        }
        
        //lancia bomba
        if (Input.GetKeyDown( KeyCode.C) )
        {
            ThrowBomb();
        }
        
        //spara bow
        if (Input.GetKeyDown(KeyCode.E))
        {
            Bow.gameObject.SetActive(true);
            Sword.gameObject.SetActive(false);
            Bow.Shoot();
        }
    }

    private void ThrowBomb()
    {
        Bomb bombObject = Instantiate(BombPrefabs);
        bombObject.transform.position = transform.position + Model.transform.forward;

        Vector3 throwingDirection = ( Model.transform.forward + Vector3.up ).normalized;
        
        bombObject.GetComponent<Rigidbody>().AddForce( throwingDirection * BombThrowingSpeed );
    }

    /// <summary>
    /// Se puo saltare fa saltare il player
    /// Controlla _canjump
    /// </summary>
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && _canJump)
        {
            _canJump = false;

            _playerRigidBody.velocity = new Vector3(
                _playerRigidBody.velocity.x,
                JumpingVelocity,
                _playerRigidBody.velocity.z
            );
        }
    }

    /// <summary>
    /// controllo con un Raycast che il player abbia toccato il pavmento
    /// solo quando non ha premuto spazio
    /// e imposto _canJump a true
    /// </summary>
    private void SetIfCanJump()
    {
        RaycastHit hit;
        if (!Input.GetKey(KeyCode.Space) && Physics.Raycast(transform.position, Vector3.down, out hit, 1.001f))
        {
            _canJump = true;
        }
    }
}