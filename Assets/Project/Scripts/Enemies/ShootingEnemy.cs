﻿using UnityEngine;

namespace Project.Scripts.Enemies
{
	public class ShootingEnemy : Enemy
	{

		public GameObject Model;
		public float TimeToRotate = 2f;
		public float RotationSpeed = 6F;

		private int _targetAngle = 0;
		private float _rotationTimer;
		
		// Use this for initialization
		void Start ()
		{
			_rotationTimer = TimeToRotate;
		}
	
		// Update is called once per frame
		void Update ()
		{
			_rotationTimer -= Time.deltaTime;
			if (_rotationTimer <= 0f)
			{
				_rotationTimer = TimeToRotate;

				_targetAngle += 90;
				
				Debug.Log(Quaternion.Euler(0, _targetAngle, 0).eulerAngles);
				
				
			}
			
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(0, _targetAngle, 0), Time.deltaTime*RotationSpeed );
		}
	}
}
