﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;

public class Sword : MonoBehaviour
{

	public float SwingingSpeed = 50f;
	public float CooldownSpeed = 1f;
	public float CooldownDuration = 0.2f;
	public float AttackDuration = 0.5f;

	public bool IsAttacking
	{
		get { return _isAttacking; }
	}

	private Quaternion _targetRotation;
	private float _cooldownTimer;
	private bool _isAttacking;
	
	// Use this for initialization
	void Start () {
		_targetRotation = Quaternion.Euler(0, 0, 0);

		_cooldownTimer = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.localRotation = Quaternion.Lerp(transform.localRotation, _targetRotation, (_isAttacking ? SwingingSpeed : CooldownSpeed) * Time.deltaTime);
		
		if (_cooldownTimer > 0f)
		{
			_cooldownTimer -= Time.deltaTime;
		}
		
	}

	public void Attack()
	{
		if (_cooldownTimer > 0f)
		{
			return;
		}


		_targetRotation = Quaternion.Euler(90, 0, 0);

		_cooldownTimer = CooldownDuration;

		StartCoroutine(CooldownWait());
	}

	private IEnumerator CooldownWait()
	{
		_isAttacking = true;
			
		yield return new WaitForSeconds(AttackDuration);

		_isAttacking = false;
		
		_targetRotation = Quaternion.Euler(0, 0, 0);
	}
}
