﻿using UnityEngine;

namespace Project.Scripts
{
	public class Arrow : MonoBehaviour
	{
		public float StartSpeed = 10f;
		public float Lifetime = 10f;
	
		// Use this for initialization
		void Start ()
		{
			GetComponent<Rigidbody>().velocity = transform.forward * StartSpeed;
		}

		void Update()
		{
			if (Lifetime <= 0)
			{
				Destroy(gameObject);
			}

			Lifetime -= Time.deltaTime;
		}
	}
}
