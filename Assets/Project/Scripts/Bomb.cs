﻿using System.Collections;
using Project.Scripts.Enemies;
using UnityEngine;

namespace Project.Scripts
{
	public class Bomb : MonoBehaviour
	{
	
		public float Duration = 5f;
		public float ExplosionRadius = 3f;
		public float ExplosionDuration = 0.25f;

		public GameObject ExplosionModel;
	
		private float _explosionTimer;
		private bool _exploded;
	
		// Use this for initialization
		void Start ()
		{
			_explosionTimer = Duration;
			ExplosionModel.transform.localScale = Vector3.one * ExplosionRadius;
			ExplosionModel.SetActive( false );
		}
	
		// Update is called once per frame
		void Update ()
		{
			_explosionTimer -= Time.deltaTime;

			if (_explosionTimer <= 0 && _exploded == false)
			{
				_exploded = true;
			
				Collider[] hitObjects = Physics.OverlapSphere(transform.position, ExplosionRadius);

				foreach (Collider hitObject in hitObjects)
				{
					if (hitObject.GetComponent<Enemy>() != null)
					{
						hitObject.GetComponent<Enemy>().Hit();
					}
				}

				StartCoroutine(Explode());
			}

		}

		private IEnumerator Explode()
		{
			ExplosionModel.SetActive(true);
		
			yield return new WaitForSeconds(ExplosionDuration);
		
			Destroy(gameObject);
		}
	}
}
